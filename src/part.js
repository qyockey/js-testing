/** object that contains a mapping between field value and attribute */
export default class Part {
  #fieldToAttribute;

  constructor(fieldToAttribute) {
    this.#fieldToAttribute = fieldToAttribute;
  }
  
  getFieldAttribute(field) {
    return this.#fieldToAttribute.get(field);
  }
}

