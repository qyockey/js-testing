import { defineConfig } from 'vite';
import babel from 'vite-plugin-babel';

// https://vitejs.dev/config
export default defineConfig({
  plugins: [
    babel({
      babelConfig: {
        babelrc: false,
        configFile: false,
        assumptions: {
          setPublicClassFields: true,
        },
        plugins: [
          [
            '@babel/plugin-proposal-decorators',
            {
              version: '2018-09',
              decoratorsBeforeExport: true,
            },
          ],
          ['@babel/plugin-proposal-class-properties'],
        ],
      },
    }),
  ],
});
