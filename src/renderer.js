/**
 * This file will automatically be loaded by vite and run in the "renderer" context.
 * To learn more about the differences between the "main" and the "renderer" context in
 * Electron, visit:
 *
 * https://electronjs.org/docs/tutorial/application-architecture#main-and-renderer-processes
 *
 * By default, Node.js integration in this file is disabled. When enabling Node.js integration
 * in a renderer process, please be aware of potential security implications. You can read
 * more about security risks here:
 *
 * https://electronjs.org/docs/tutorial/security
 *
 * To enable Node.js integration in this file, open up `main.js` and enable the `nodeIntegration`
 * flag:
 *
 * ```
 *  // Create the browser window.
 *  mainWindow = new BrowserWindow({
 *    width: 800,
 *    height: 600,
 *    webPreferences: {
 *      nodeIntegration: true
 *    }
 *  });
 * ```
 */

import './index.css';
import AppContainer from './components/app-container';

console.log(AppContainer)
import { Parser } from './parser.js';

console.log('👋 This message is being logged by "renderer.js", included via Vite');

let header;
let allParts;

// is there a way to run this only when on spreadsheet-options.html page?
/* const parseBtn = document.getElementById("parseBtn");
 * parseBtn.addEventListener('click', async () => {
 *   const file = document.getElementById('fileSelector').files[0];
 *   const hasHeader = document.getElementById('headerCheckbox').checked;
 *   const delimiter = document.getElementById('delimiterInput').value;
 *   if (file === undefined) {
 *     alert('Please enter a csv file.');
 *   } else if (delimiter === '') {
 *     alert('Please enter a delimiter.');
 *   } else {
 *     let parser = new Parser();
 *     await parser.parsePartsCsv(file, hasHeader, delimiter);
 *     header = parser.getHeader();
 *     allParts = parser.getAllParts();
 *     console.log(header, allParts);
 *   }
 * });
 */
