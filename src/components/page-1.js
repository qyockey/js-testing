import { LitElement, html } from "lit"
import { customElement, query, property } from 'lit/decorators.js';

@customElement('page-1')
export default class Page1 extends LitElement {

  render() {
    return html`
      <div>
        Select your file:
        <input type='file'></div>
        <a href='/page-2'>Next</a>
      </div>
    `
  }

  createRenderRoot() {
    return this;
  }
}
