import { LitElement, html } from "lit"
import { customElement, query, property } from 'lit/decorators.js';

@customElement('page-2')
export default class Page2 extends LitElement {

  render() {
    return html`
      <div>Label output result!</div>
      <a href='/page-1'>Restart</a>
    `
  }

  createRenderRoot() {
    return this;
  }
}
