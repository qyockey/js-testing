import { LitElement, html } from "lit";
import { customElement, query, property } from 'lit/decorators.js';
import { Router } from '@vaadin/router';

import './page-1.js';
import './page-2.js';

@customElement('app-container')
export default class AppContainer extends LitElement {

  @query('content.outlet') outlet;

  firstUpdated() {
    super.firstUpdated();

    const router = new Router(this.outlet);
    router.setRoutes([
      { path: '/page-1', component: 'page-1' },
      { path: '/page-2', component: 'page-2' },
      { path: '(.*)', redirect: '/page-1' },
    ]);
  }

  render() {
    return html`
      <header>Label Maker</header>
      <content class='outlet'></content>
    `
  }

  createRenderRoot() {
    return this;
  }
}
